# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.12

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.2.11

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.2.10

- patch: Add warning message about new version of the pipe available.

## 0.2.9

- patch: Added code style checks

## 0.2.8

- patch: Internal maintenance: update pipes toolkit version.

## 0.2.7

- patch: Add git and hg binaries to build container

## 0.2.6

- patch: Minor documentation updates

## 0.2.5

- patch: Fixed the issue with passing multiple DISTRIBUTIONS to the pipe

## 0.2.4

- patch: Updated contributing guidelines

## 0.2.3

- patch: Fix colourised logging.

## 0.2.2

- patch: Fix the default PyPI URL

## 0.2.1

- patch: Fixed the default PyPI URL

## 0.2.0

- minor: Fix docker image name in the release process

## 0.1.0

- minor: Initial release
- minor: Initial release of Bitbucket Pipelines for publishing to PyPI

